#define init
global.sprPopCannon = sprite_add_weapon("sprites/sprPopCannon.png", 3, 5);

#define weapon_name
return "POP CANNON";

#define weapon_text
return "5/7";

#define weapon_sprt
return global.sprPopCannon;

#define weapon_type
return 1;

#define weapon_cost
return 8;

#define weapon_load
return 35;

#define weapon_swap
return sndSwapExplosive;

#define weapon_area
return 8;

#define weapon_fire
sound_play_gun(sndFlakCannon, 0.25, 0.3);

with(instance_create(x + lengthdir_x(skill_get(13)*14, aimDirection), y + lengthdir_y(skill_get(13)*14, aimDirection), CustomProjectile))
{
	name = "PopCannon";
	sprite_index = sprFlakBullet;
	mask_index = mskFlakBullet;
	hitid = [sprFlakBullet, "POP CANNON"];
	image_speed = 0.04;
	creator = other.id;
	team = other.team;
	damage = 20;
	force = 6;
	typ = 1;
	alarm0 = 180;

	motion_add(other.aimDirection + (random(12)-6)*other.accuracy, 4);
	image_angle = direction;
	
	on_step = popcan_step;
	on_hit = popcan_hit;
	on_wall = popcan_wall;
	on_draw = popcan_draw;
	on_destroy = popcan_destroy;
}

weapon_post(12, 40, 8);

#define popcan_draw
mod_script_call("mod", "chaoslibrary", "bloom_draw");
draw_self();

#define popcan_step
if(speed <= 0)
instance_destroy();

image_speed = speed/12;

if(current_frame mod 2 = 1)
sound_play(sndPopgun);

with(instance_create(x, y, Bullet2))
{
	direction = other.image_angle;
	team = other.team;
	motion_add(direction, 12);
}
image_angle += 125;

view_shake_at(x, y, 1);

if(alarm0 = 0)
instance_destroy();

#define popcan_hit
if(projectile_canhit(other))
{
	sleep(100);
	projectile_hit(other, damage, force, direction);
	instance_destroy();
}

#define popcan_destroy
sound_play(sndEraser);

repeat(10)
{
	with instance_create(x,y,Bullet2)
	{
		motion_add(random(360),8+random(8));
		image_angle = direction;
		team = other.team;
	}
}

sleep(20);

repeat(10)
{
	with(instance_create(x,y,Smoke))
	motion_add(random(360),random(3));
}

view_shake_at(x, y, 8);

#define popcan_wall
sound_play(sndHitWall);
instance_destroy();
