#define init
global.sprUltraScrew = sprite_add_weapon("sprites/sprUltraScrewdriver.png", -1, 2);
global.sprUltraShank = sprite_add("sprites/sprUltraShank.png", 2, -5, 8);

#define weapon_name
return "ULTRA SCREWDRIVER";

#define weapon_text
return "FIX ALL OF YOUR PROBLEMS";

#define weapon_sprt
return global.sprUltraScrew;

#define weapon_type
return 0;

#define weapon_melee
return 1;

#define weapon_cost
return 0;

#define weapon_load
return 13;

#define weapon_swap
return sndSwapSword;

#define weapon_area
return 23;

#define weapon_fire
if(GameCont.rad >= 16)
{
	sound_play_gun(sndScrewdriver, 0.25, 0.6);
	sound_play_gun(sndUltraShovel, 0.25, 0.6);
	
	GameCont.rad -= 16;

	with(instance_create(x + lengthdir_x(skill_get(13)*14, aimDirection), y + lengthdir_y(skill_get(13)*14, aimDirection), CustomProjectile))
	{
		name = "UltraScrew";
		sprite_index = global.sprUltraShank;
		mask_index = sprite_index;
		hitid = [global.sprUltraShank, "ULTRA SHANK"];
		image_speed = 0.4;
		creator = other.id;
		team = other.team;
		damage = 30;
		force = 6;
		typ = 0;

		motion_add(other.aimDirection + (random(10)-5)*other.accuracy, 3 + skill_get(13));
		image_angle = direction;
		
		on_step = chain_step;
		on_hit = chain_hit;
		on_wall = chain_wall;
		on_anim = chain_anim;
	}

	wepangle = -wepangle;
	weapon_post(-8, 12, 1);
}
else
{
	clicked = 0;
	sound_play(sndEmpty);
	dir = instance_create(x,y,PopupText);
	dir.mytext = "NOT ENOUGH RADS";
}

#define chain_anim
instance_destroy();
#define chain_step
image_angle = direction;
with(projectile)
{
	if(place_meeting(x, y, other) and other.team != team and (typ = 2 or typ = 1))
	{
		instance_destroy();
	}
}

#define chain_hit
if(projectile_canhit_melee(other))
{
	projectile_hit(other, damage, force, direction);
	var new_screw;
	near_enemy = instance_nearest(other.x + lengthdir_x((speed * 3) + (skill_get(13) * 14), direction), other.y + lengthdir_y((speed * 3) + (skill_get(13) * 14), direction), enemy);
	new_screw = instance_copy(false);
	new_screw.image_index = 0;
	new_screw.direction = point_direction(other.x, other.y, near_enemy.x, near_enemy.y);
	new_screw.x = other.x + lengthdir_x(skill_get(13)*14, new_screw.direction);
	new_screw.y = other.y + lengthdir_y(skill_get(13)*14, new_screw.direction);
}


#define chain_wall
x -= hspeed;
y -= vspeed;
