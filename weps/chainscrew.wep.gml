#define init
global.sprChainScrew = sprite_add_weapon("sprites/sprChainScrew.png", -1, 2);

#define weapon_name
return "CHAIN SCREWDRIVER";

#define weapon_text
return "MODERN TECHNOLOGY";

#define weapon_sprt
return global.sprChainScrew;

#define weapon_type
return 0;

#define weapon_melee
return 1;

#define weapon_cost
return 0;

#define weapon_load
return 35;

#define weapon_swap
return sndSwapSword;

#define weapon_area
return 7;

#define weapon_fire
sound_play_gun(sndScrewdriver, 0.25, 0.6);

with(instance_create(x + lengthdir_x(skill_get(13)*14, aimDirection), y + lengthdir_y(skill_get(13)*14, aimDirection), CustomProjectile))
{
	name = "ChainScrew";
	sprite_index = sprShank;
	mask_index = sprShank;
	hitid = [sprShank, "CHAIN SHANK"];
	image_speed = 0.4;
	creator = other.id;
	team = other.team;
	damage = 6;
	force = 6;
	typ = 0;

	motion_add(other.aimDirection + (random(10)-5)*other.accuracy, 3 + skill_get(13));
	image_angle = direction;
	
	on_step = chain_step;
	on_hit = chain_hit;
	on_wall = chain_wall;
	on_anim = chain_anim;
}

wepangle = -wepangle;
weapon_post(-8, 12, 1);

#define chain_anim
instance_destroy();

#define chain_step
image_angle = direction;
with(projectile)
{
	if(place_meeting(x, y, other) and other.team != team and (typ = 2 or typ = 1))
	{
		instance_destroy();
	}
}

#define chain_hit
if(projectile_canhit_melee(other))
{
	projectile_hit(other, damage, force, direction);
	var new_screw;
	near_enemy = instance_nearest(other.x + lengthdir_x((speed * 3) + (skill_get(13) * 14), direction), other.y + lengthdir_y((speed * 3) + (skill_get(13) * 14), direction), enemy);
	new_screw = instance_copy(false);
	new_screw.image_index = 0;
	new_screw.direction = point_direction(other.x, other.y, near_enemy.x, near_enemy.y);
	new_screw.x = other.x + lengthdir_x(skill_get(13)*14, new_screw.direction);
	new_screw.y = other.y + lengthdir_y(skill_get(13)*14, new_screw.direction);
}


#define chain_wall
x -= hspeed;
y -= vspeed;
