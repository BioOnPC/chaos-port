#define init
global.sprSeekerSMG = sprite_add_weapon("sprites/sprSeekerSMG.png", 4, 4);

#define weapon_name
return "SEEKER SMG";

#define weapon_text
return "no hiding";

#define weapon_sprt
return global.sprSeekerSMG;

#define weapon_type
return 3;

#define weapon_auto
return 1;

#define weapon_cost
return 1;

#define weapon_load
return 8;

#define weapon_swap
return sndSwapBow;

#define weapon_area
return 11;

#define weapon_fire
sound_play_gun(sndSeekerPistol, 0.25, 0.6);

repeat(2)
{
	with(instance_create(x, y, Seeker))
	{
		motion_add(other.aimDirection+(random(100)-50)*other.accuracy,4.5+random(1));
		image_angle = direction;
		team = other.team;
	}
}

weapon_post(4, 0, 2);
