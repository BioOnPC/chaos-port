#define init
global.sprAFDoubleShotgun = sprite_add_weapon("sprites/sprAFDoubleShotgun.png", 4, 3);

#define weapon_name
return "A2F";

#define weapon_text
return "cremate them";

#define weapon_sprt
return global.sprAFDoubleShotgun;

#define weapon_type
return 2;

#define weapon_auto
return 1;

#define weapon_cost
return 2;

#define weapon_load
return 8;

#define weapon_swap
return sndSwapShotgun;

#define weapon_area
return 14;

#define weapon_fire
player_fire_ext(aimDirection, 76, x, y, team, id);

weapon_post(5, 15, 12);
