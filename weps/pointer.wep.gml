#define init
global.sprLaserPointer = sprite_add_weapon("sprites/sprLaserPointer.png", -1, 2);

#define weapon_name
return "LASER POINTER";

#define weapon_text
return "";

#define weapon_sprt
return global.sprLaserPointer;

#define weapon_type
return 0;

#define weapon_cost
return 0;

#define weapon_load
return 0;

#define weapon_swap
return sndSniperTarget;

#define weapon_area
return -1;

#define weapon_laser_sight
return 1;

#define weapon_melee
return 0;
